﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using ModelMaterialFix.MaterialFix;
using ModelMaterialFix.SceneformSFA;
using Path = System.IO.Path;

namespace ModelMaterialFix
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowModel model;
        public MainWindow()
        {
            InitializeComponent();
            model = (MainWindowModel) DataContext;

            var files = Directory.GetFiles(Directory.GetCurrentDirectory());
            bool singleObj = files.Count(x => x.EndsWith(".obj")) == 1;
            if (singleObj)
            {
                var file = files.First(x => x.EndsWith(".obj"));
                model.Material.ObjectFileName = file.Substring(model.CurrentDir.Length);
                autoloadMtl(file);
            }
        }


        private void selectObjFile(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenFileDialog();
            dlg.Filter = "Object model file (*.obj)|*.obj";
            dlg.InitialDirectory = model.CurrentDir;
            dlg.CheckFileExists = true;

            if (dlg.ShowDialog() != true)
                return;

            model.Material.ObjectFileName = dlg.FileName.Substring(model.CurrentDir.Length);

            string newDir = Path.GetDirectoryName(dlg.FileName);
            if (newDir != model.CurrentDir)
                model.CurrentDir = newDir;
            autoloadMtl(dlg.FileName);
        }

        private void autoloadMtl(string fileName)
        {
            using (var reader = new StreamReader(fileName))
            {
                string l;
                while ((l = reader.ReadLine()) != null)
                {
                    if (l.Contains("mtllib"))
                    {
                        int index = l.IndexOf("mtllib") + 6;
                        string name = l.Substring(index).Trim();
                        string mtlFileName = model.CurrentDir + "\\" + name;
                        if (File.Exists(mtlFileName))
                            model.Material.MaterialFileName = name;
                        break;
                    }
                }
            }
        }

        private void selectMatFile(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenFileDialog();
            dlg.Filter = "Wavefront material file (*.mtl)|*.mtl";
            dlg.InitialDirectory = Directory.GetCurrentDirectory();
            dlg.CheckFileExists = true;

            if (dlg.ShowDialog() != true)
                return;

            model.Material.MaterialFileName = dlg.FileName.Substring(model.CurrentDir.Length);
        }

        

        private void applyMaterialFix(object sender, RoutedEventArgs e)
        {
            // Apply fix from 
            var res = MessageBox.Show("Delete original files?", "",
                MessageBoxButton.YesNo, MessageBoxImage.Question);
            bool deleteOriginalFiles = res == MessageBoxResult.Yes;

            string dir = model.CurrentDir + "\\";
            string materialFile = dir + model.Material.MaterialFileName;
            FixManager.FixMaterial(!deleteOriginalFiles, materialFile, 
                model.Material.Textures.Select(x=>model.CurrentDir + x).ToArray());

            MessageBox.Show("Material fixed");
        }


        private void selectTextures(object sender, RoutedEventArgs e)
        {

            var dlg = new OpenFileDialog();
            dlg.Filter = "Material textures (*.jpg;*.png;*.bmp)|*.jpg;*.jpeg;*.png;*.bmp;*.gif|All files (*.*)|*.*";
            dlg.InitialDirectory = Directory.GetCurrentDirectory();
            dlg.CheckFileExists = true;
            dlg.Multiselect = true;

            if (dlg.ShowDialog() != true)
                return;
            var list = new List<string>();
            foreach(var f in dlg.FileNames)
            {
                string s = f.Substring(model.CurrentDir.Length);
                list.Add(s);
            }
            model.Material.Textures = list;
        }

        private void updateName(object sender, RoutedEventArgs e)
        {

            string fileName = newFileName.Text;
            {
                // Remove extensions.
                int i = fileName.IndexOf(".");
                if (i >= 0)
                    fileName = fileName.Substring(0, i);
            }

            string dir = model.CurrentDir + "\\";

            FixManager.UpdateName(dir, fileName, model.Material);

        }

        private void autoload(object sender, RoutedEventArgs e)
        {
            string dir = model.CurrentDir + "\\";

        }

        private void selectFileSFA(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenFileDialog();
            dlg.Filter = "SFA file (*.sfa)|*.sfa;";
            dlg.InitialDirectory = model.CurrentDir;
            dlg.CheckFileExists = true;

            if (dlg.ShowDialog() != true)
                return;

            var materials = SFAManager.GetMaterials(dlg.FileName);
            model.Model.FileSFA = dlg.FileName;
            model.Model.Materials = materials;
        }

        private void updateRoughness(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // Update roughness to all selected materials.
            var materials = materialsGrid.SelectedItems.Cast<SingleMaterial>();
            foreach (var m in materials)
                m.Roughness.Value = (float)e.NewValue;
        }
        private void metalicChecked(object sender, RoutedEventArgs e)
        {
            checkedChanged(true);
        }

        private void metalicUnchecked(object sender, RoutedEventArgs e)
        {
            checkedChanged(false);
        }
        private void checkedChanged(bool newValue)
        {
            var materials = materialsGrid.SelectedItems.Cast<SingleMaterial>();
            foreach (var m in materials)
                m.Metalic.Value = newValue ? 1.0f : 0.0f;

        }

        private void sfaMaterialFix(object sender, RoutedEventArgs e)
        {

            var dlg = new OpenFileDialog();
            dlg.Filter = "SFA file (*.sfa)|*.sfa;";
            dlg.InitialDirectory = model.CurrentDir;
            dlg.CheckFileExists = true;

            if (dlg.ShowDialog() != true)
                return;

            SFAManager.FixMaterials(dlg.FileName);
        }

    }
}
