﻿using ModelMaterialFix.MaterialFix;
using ModelMaterialFix.SceneformSFA;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelMaterialFix
{
    class MainWindowModel : Notify
    {
        public string CurrentDir
        {
            get => _currentDir;
            set => OnPropertyChanged(()=>
            {
                _currentDir = value;
                Material = new MaterialFixModel();
                Model = new SceneformModel();
            });
        }

        public MaterialFixModel Material
        {
            get => _material;
            set => OnPropertyChanged(()=> _material = value);
        }

        public SceneformModel Model
        {
            get => _model;
            set => OnPropertyChanged(()=> _model = value);
        }

        private MaterialFixModel _material;
        private SceneformModel _model;
        private string _currentDir;

        public MainWindowModel()
        {
            CurrentDir = Directory.GetCurrentDirectory();
            Model = new SceneformModel();
            Material = new MaterialFixModel();
        }
    }
}
