﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelMaterialFix.SceneformSFA
{
    class SupportedMaterialParameters
    {
        public const int MetalicIndex = 0;
        public const int RoughnessIndex = 1;
        public static Dictionary<string, int> Parameters = new Dictionary<string, int>()
        {
            ["metallic"] = MetalicIndex,
            ["roughness"] = RoughnessIndex
        };
    }
    class MaterialParameter<T> : Notify where T:struct 
    {
        public string Name{ get; }
        public T Value 
        {
            get => _value;
            set => OnPropertyChanged(() => _value = value);
        }

        public int LineIndex{ get; set; }


        private T _value;
        public MaterialParameter(string name, T value, int lineIndex)
        {
            Name = name;
            Value = value;
            LineIndex = lineIndex;
        }
    }
    class SingleMaterial
    {
        public string Name { get; }
        public string Content{ get; private set; }
        public MaterialParameter<float> Metalic { get; private set; }
        public MaterialParameter<float> Roughness { get; private set; }

        private readonly List<MaterialParameter<float>> parameters = new List<MaterialParameter<float>>();
        private string[] lines = null;
        public SingleMaterial(string name, string content)
        {
            Name = name;
            Content = content;
            readParameters();
        }

        private void readParameters()
        {
            lines = Content.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                string s = lines[i];
                int indexOf = s.IndexOf(':');
                if (indexOf == -1)
                    continue;
                string sub = s.Substring(0, indexOf);
                if (SupportedMaterialParameters.Parameters.ContainsKey(sub))
                {
                    float f = 0;
                    string subValue = s.Substring(indexOf + 1);
                    if (float.TryParse(subValue, out f))
                    {
                        var p = new MaterialParameter<float>(sub, f, i);
                        parameters.Add(p);
                        switch (SupportedMaterialParameters.Parameters[sub])
                        {
                            case SupportedMaterialParameters.MetalicIndex:
                                Metalic = p;
                                break;
                            case SupportedMaterialParameters.RoughnessIndex:
                                Roughness = p;
                                break;
                        }
                    }
                    break;
                }
            }
        }

        public void SetParameter(string parameterName, float newValue)
        {
            var p = parameters.FirstOrDefault(x => x.Name == parameterName);
            if (p == null)
                return;
            p.Value = newValue;
            lines[p.LineIndex] = $"{parameterName} : {newValue:0.0},";
            Content = string.Join("\n", lines);
        }
    }
}
