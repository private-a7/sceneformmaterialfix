﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelMaterialFix.SceneformSFA
{
    class SceneformModel : Notify
    {
        public string FileSFA
        {
            get => _fileSFA;
            set => OnPropertyChanged(() => _fileSFA = value);
        }

        public List<SingleMaterial> Materials
        {
            get => _materials;
            set => OnPropertyChanged(() => _materials = value);
        }

        private string _fileSFA;
        private List<SingleMaterial> _materials;
    }
}
