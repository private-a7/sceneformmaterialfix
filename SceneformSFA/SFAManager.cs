﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelMaterialFix.SceneformSFA
{
    class SFAManager
    {
        public static List<SingleMaterial> GetMaterials(string sfaFileName)
        {
            var materials = new List<SingleMaterial>();

            JObject obj = JObject.Parse(sfaFileName);
            JArray materialsArray = (JArray)obj["materials"];
            foreach(var m in materialsArray)
            {
                var nameToken = m["name"];
                string name = nameToken.Value<string>();

                var mat = new SingleMaterial(name, m.Value<string>());
                materials.Add(mat);
            }
            return materials;
        }

        /// <summary>
        /// SFA is kind of JSON, but not quite there (parameter name is not escaped),
        /// and JSON parser doesn't work (maybe it could, but need to explore that 
        /// </summary>
        /// <param name="sfaFileName"></param>
        public static void FixMaterials(string sfaFileName)
        {
            string[] lines = File.ReadAllLines(sfaFileName);

            bool isInox = false;
            for (int i = 0; i < lines.Length; i++)
            {
                var l = lines[i];
                if (l.Contains("name"))
                {
                    if (l.Contains("inox"))
                        isInox = true;
                    else
                        isInox = false;
                }
                if (l.Contains("metallic"))
                {
                    int index = l.IndexOf("metallic");
                    string s = l.Substring(0, index) + "metallic: ";
                    if (isInox)
                        s += "1,";
                    else
                        s += "0,";
                    lines[i] = s;
                }

                if (l.Contains("scale"))
                {
                    int index = l.IndexOf("scale");
                    string s = l.Substring(0, index) + "scale: 1,";
                    lines[i] = s;
                }
            }

            File.WriteAllLines(sfaFileName, lines);
        }
    }
}
