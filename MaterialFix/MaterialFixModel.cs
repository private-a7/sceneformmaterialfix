﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelMaterialFix.MaterialFix
{
    class MaterialFixModel : Notify
    {
        public bool ObjectFileSelected
        {
            get => _objectFileSelected;
            set => OnPropertyChanged(()=> _objectFileSelected = value);
        }

        public string ObjectFileName
        {
            get => _objectFileName;
            set => OnPropertyChanged(()=>
            {
                _objectFileName = value;
                OnPropertyChanged(nameof(CanRename));
                OnPropertyChanged(nameof(CanApply));
                ObjectFileSelected = true;
            });
        }

        public string MaterialFileName
        {
            get => _materialFileName;
            set => OnPropertyChanged(()=>
            {
                _materialFileName = value;
                OnPropertyChanged(nameof(CanRename));
                OnPropertyChanged(nameof(CanApply));
            });
        }

        public List<string> Textures
        {
            get => _textures;
            set => OnPropertyChanged(()=>
            {
                _textures = value;
                OnPropertyChanged(nameof(CanApply));
            });
        }

        /// <summary>
        /// Rename is possible when both object and material are selected.
        /// </summary>
        public bool CanRename
            => !string.IsNullOrEmpty(ObjectFileName)
               && !string.IsNullOrEmpty(MaterialFileName);

        /// <summary>
        /// Apply can be done when material file and textures
        /// are selected.
        /// </summary>
        public bool CanApply
            => !string.IsNullOrEmpty(MaterialFileName)
               && Textures.Any();

        private string _objectFileName = "";
        private string _materialFileName = "";
        private List<string> _textures = new List<string>();
        private bool _objectFileSelected;
    }
}
