﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ModelMaterialFix.MaterialFix
{
    class FixManager
    {

        public static void FixMaterial(bool keepOriginal, string materialFile, string[] textureFiles)
        {
            // Old name, New name, full path.
            var renamed = new[]
            {
                new
                {
                    Name = "",
                    NewName = "",
                    Path = ""
                }
            }.ToList();
            renamed.Clear();
            // Works only with texture files in same directory as material file.
            foreach (var s in textureFiles)
            {
                string name = Path.GetFileName(s);
                string newName = name.Replace(' ', '-');
                if (name != newName)
                {
                    string path = Path.GetDirectoryName(s) + "\\";
                    renamed.Add(new { Name = name, NewName = newName, Path = path });
                }
            }

            // Rename files.
            foreach (var t in renamed)
            {
                string name = t.Name;
                string newName = t.NewName;
                string path = t.Path;
                if (File.Exists(path + newName))
                    File.Delete(path + newName);
                if (keepOriginal)
                {
                    File.Copy(path + name, path + newName);
                }
                else
                    File.Move(path + name, path + newName);
            }

            // Process them in material file.
            string[] material = File.ReadAllLines(materialFile);
            // Find only rows starting with map.
            //material = material.Where(x => x.TrimStart().StartsWith("map"))
            //.ToArray();
            for (int i = 0; i < material.Length; i++)
            {
                string row = material[i];
                if (!row.TrimStart().StartsWith("map"))
                    continue;
                foreach (var e in renamed)
                {
                    string newRow = row.Replace(e.Name, e.NewName);
                    if (row != newRow)
                    {
                        material[i] = newRow;
                        break;
                    }
                }
            }

            if (renamed.Any())
            {
                string s = materialFile + ".old";
                if (File.Exists(s))
                    File.Delete(s);
                File.Move(materialFile, s);
                File.WriteAllLines(materialFile, material);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentDir">With end backslash</param>
        /// <param name="fileName">Name without extension</param>
        /// <param name="model"></param>
        public static void UpdateName(string currentDir, string fileName, MaterialFixModel model)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                MessageBox.Show("Invalid file");
                return;
            }
            var res = MessageBox.Show("Delete original files?", "",
                MessageBoxButton.YesNo, MessageBoxImage.Question);
            bool deleteOriginalFiles = res == MessageBoxResult.Yes;

            string newMaterialName = currentDir + fileName + ".mtl";
            string newObjectName = currentDir + fileName + ".obj";
            string oldMaterialName = currentDir + model.MaterialFileName;
            string oldObjectName = currentDir + model.ObjectFileName;

            // Rename material file.
            File.Copy(oldMaterialName, newMaterialName);
            // Rename object file.
            //File.Copy(oldObjectName, newObjectName);

            // Rename material file name in object file.
            using (var reader = new StreamReader(oldObjectName))
            using (var writer = new StreamWriter(newObjectName))
            {
                string line;
                bool done = false;
                while ((line = reader.ReadLine()) != null)
                {
                    string s = line;
                    if (!done && s.Contains("mtllib"))
                    {
                        s = "mtllib " + fileName + ".mtl";
                        done = true;
                    }
                    writer.WriteLine(s);
                }
            }

                model.MaterialFileName = fileName + ".mtl";
            model.ObjectFileName = fileName + ".obj";
            if (deleteOriginalFiles)
            {
                try
                {
                    File.Delete(oldMaterialName);
                    File.Delete(oldObjectName);
                }
                catch
                { }
            }
        }
    }
}
