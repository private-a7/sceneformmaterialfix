﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ModelMaterialFix
{
    public class Notify : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        protected void OnPropertyChanged(Action a, [CallerMemberName] string name = null)
        {
            a();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        protected void OnPropertyChanged<T>(ref T field, T val, [CallerMemberName] string name = null)
        {
            field = val;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
